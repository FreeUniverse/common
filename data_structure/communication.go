package data_structure

type Detail interface{}

type message struct {
	Type     string                  `json:"Type"`
	Request  *map[string]interface{} `json:"Request,omitempty"`
	Response *map[string]interface{} `json:"Response,omitempty"`
}

type ConsultRequest struct {
	Target []string
}

type ConsultResponse struct {
	Planets []PlanetInfo `json:"Planets,omitempty"`
}

type PlanetInfo struct {
	Name       string  `json:"Name" yaml:"Name"`
	ReviseDate string  `json:"Revised,omitempty" yaml:"Revised,omitempty"`
	Radius     float32 `json:"Radius" yaml:"Radius"`
	Mass       string  `json:"Mass" yaml:"Mass"`
	Center     string  `json:"Center" yaml:"Center,omitempty"`
}

type PlanetRequest struct {
	Time string
	Name string
}

type PlanetResponse struct {
	Time string
	Planet
}

type UniverseRequest struct {
	Time   string
	Planet []string
}

type UniverseResponse Universe
