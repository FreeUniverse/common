// 程序中用到的數據結構
package data_structure

type Universe struct {
	Time    string
	Planets []Planet
}

/*
星球信息：
	* 名稱
    * 坐標
	* 自轉傾角
	* 角度（自轉）
*/
type Planet struct {
	Name  string
	Coord Coordinate
}

type Coordinate struct {
	X float32
	Y float32
	Z float32
}

func NewCoord(x, y, z float32) Coordinate {
	return Coordinate{x, y, z}
}
