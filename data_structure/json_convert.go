package data_structure

import (
	"encoding/json"
	"errors"
	"io"
)

func mapToStruct(m *map[string]interface{}, st interface{}) error {
	dre, err := json.Marshal(m)
	if err != nil {
		return err
	}
	err = json.Unmarshal(dre, st)
	if err != nil {
		return err
	}
	return nil
}

func structToMap(st interface{}) (*map[string]interface{}, error) {
	dre, err := json.Marshal(st)
	if err != nil {
		return nil, err
	}
	var m map[string]interface{}
	err = json.Unmarshal(dre, &m)
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func ParseMsg(r io.Reader) (Detail, error) {
	decoder := json.NewDecoder(r)
	var msg message
	if decoder.More() {
		err := decoder.Decode(&msg)
		if err != nil {
			panic(err)
		}
		if msg.Request == nil && msg.Response == nil {
			return nil, errors.New("不合法傳入|無請求和回應")
		} else if msg.Request != nil && msg.Response != nil {
			return nil, errors.New("不合法傳入|請求且回應")
		}
		var ret interface{}
		switch msg.Type {
		case "Universe":
			if msg.Request != nil {
				var re UniverseRequest
				err = mapToStruct(msg.Request, &re)
				ret = re
			} else {
				var re UniverseResponse
				err = mapToStruct(msg.Response, &re)
				ret = re
			}
		case "Planet":
			if msg.Request != nil {
				var re PlanetRequest
				err = mapToStruct(msg.Request, &re)
				ret = re
			} else {
				var re PlanetResponse
				err = mapToStruct(msg.Response, &re)
				ret = re
			}
		case "Consult":
			if msg.Request != nil {
				var re ConsultRequest
				err = mapToStruct(msg.Request, &re)
				ret = re
			} else {
				var re ConsultResponse
				err = mapToStruct(msg.Response, &re)
				ret = re
			}
		default:
			return nil, errors.New("不合法傳入|類型:" + msg.Type)
		}
		return ret, err
	}
	return nil, errors.New("無效請求")
}

func ToMsg(d Detail) []byte {
	var msg message
	var err error
	switch d.(type) {
	case UniverseRequest:
		msg.Type = "Universe"
		msg.Request, err = structToMap(d)
	case UniverseResponse:
		msg.Type = "Universe"
		msg.Response, err = structToMap(d)
	case PlanetRequest:
		msg.Type = "Planet"
		msg.Request, err = structToMap(d)
	case PlanetResponse:
		msg.Type = "Planet"
		msg.Response, err = structToMap(d)
	case ConsultRequest:
		msg.Type = "Consult"
		msg.Request, err = structToMap(d)
	case ConsultResponse:
		msg.Type = "Consult"
		msg.Response, err = structToMap(d)
	default:
		err = errors.New("未知類型")
	}
	if err != nil {
		panic(err)
	}
	data, err := json.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return data
}
