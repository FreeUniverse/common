package utils

import (
	"bufio"
	"io"
	"log"
)

func ReadData(s io.Reader) []byte {
	buffer := make([]byte, 1024)
	n, err := bufio.NewReader(s).Read(buffer)
	if err != nil {
		log.Println("讀取數據錯誤：", err)
	}
	log.Printf(" {GOT} %d: %s\n", n, buffer[0:n])
	data := buffer[0:n]
	return data
}
