package utils

import (
	"strconv"
	"strings"
)

func isLeapYear(year int) bool {
	if year%100 == 0 {
		if year%400 == 0 {
			return true
		}
		return false
	}
	if year%4 == 0 {
		return true
	}
	return false
}

func IsValidTime(time string) bool {
	day_of_month := [12]int{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
	blocks := strings.Split(time, "-")
	if len(blocks) < 3 {
		return false
	}
	year, err := strconv.Atoi(blocks[0])
	if err != nil {
		return false
	}
	if year == 0 {
		return false
	}
	month, err := strconv.Atoi(blocks[1])
	if err != nil {
		return false
	}
	if month < 0 || month > 12 {
		return false
	}
	day, err := strconv.Atoi(blocks[2])
	if err != nil {
		return false
	}
	days := day_of_month[month-1]
	if isLeapYear(year) {
		days += 1
	}
	if day < 0 || day > days {
		return false
	}
	return true
}
