REST式協議說明
- - - - - - - -

* /planet_list
  所有星球
* /info/P
  P星球的信息
* /planet/P/T
  P星球在T時刻的位置
* /universe/T
  T時刻所有星球位置
  *Viewer <-> Waiter*
